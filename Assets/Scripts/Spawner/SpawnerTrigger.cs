using UnityEngine;
using System.Collections;

public class SpawnerTrigger : MonoBehaviour {
	public GameObject villain;
	private VillainAttributes va;
	
	private Spawner spawner;
	public GameObject puppet;
	
	public GameObject globalVariables;
	public HazardList hl;
	public ItemSelector isel;
	
	void Start () {
		spawner = (Spawner)transform.FindChild("Spawner").GetComponent("Spawner");
		
		va = (VillainAttributes)villain.GetComponent("VillainAttributes");
		hl = (HazardList)globalVariables.GetComponent("HazardList");
		isel = (ItemSelector)globalVariables.GetComponent("ItemSelector");
	}
	
	void Update () {
		if(Input.GetMouseButtonDown(0)) {
			if(va.DetractMoney((int)hl.listOfHazards[((IID)isel.selected.GetComponent("IID")).GetID()].cost)) {
				Ray ray = camera.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit = default(RaycastHit);
				
				if(Physics.Raycast(ray, out hit)) {
					if(hit.collider.name.Equals("Spawner Collider")) {
						spawner.AddToSpawnQueue(new OTSContainer(hit.point.y, 0.5f, isel.selected));
					}
				}
			}
			else {
				audio.Play();	
			}
		}
	}
}
