using UnityEngine;

public class OTSContainer {
	public float spawnHeight { get; private set; }
	public float delayTillNext { get; private set; }
	public GameObject prefabToSpawn { get; private set; }
	
	public OTSContainer(float sH, float dTN, GameObject pTS) {
		spawnHeight = sH;
		delayTillNext = dTN;
		prefabToSpawn = pTS;
	}
}