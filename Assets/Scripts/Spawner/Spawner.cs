using UnityEngine;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {	
	private Queue<OTSContainer> toSpawn;
	public GameObject hazardsContainer;
	
	private float delay;
	private float elapsedTime;
	
	void Start () {
		toSpawn = new Queue<OTSContainer>();
	}
	
	void Update () {
		if(toSpawn.Count > 0) {
			if(elapsedTime >= delay) {
				SpawnHazard();
				elapsedTime = 0;
			}
			else
				elapsedTime += Time.deltaTime;
		}
	}
	
	public void AddToSpawnQueue(OTSContainer container) {
		toSpawn.Enqueue(container);
	}
	
	private void SpawnHazard() {
		OTSContainer container = toSpawn.Dequeue();
		
		Vector3 spawnPosition = transform.position;
		spawnPosition.y = container.spawnHeight;
		
		delay = container.delayTillNext;
		
		GameObject justSpawned = (GameObject)Instantiate(container.prefabToSpawn, spawnPosition, container.prefabToSpawn.transform.rotation);
		justSpawned.transform.parent = hazardsContainer.transform;
	}
}
