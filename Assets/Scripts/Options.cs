using UnityEngine;

public static class Options {
	public static float musicVolume = 0.7f;
	public static float sfxVolume = 0.3f;
}
