using UnityEngine;
using System.Collections;

public class EndingScript : MonoBehaviour {
	public GameObject[] toActivate;
	public GameObject[] toDeactivate;
	
	public Timer timer;
	public GameObject timerCopy;
	
	public GameObject audioSrc;
	
	void Update() {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			Application.LoadLevel("main");	
		}	
	}
	
	void OnEnable() {
		timerCopy.guiText.text = timer.ToString();
		for(int i=0; i<toDeactivate.Length; i++) {
			toDeactivate[i].SetActive(false);
		}
		for(int i=0; i<toActivate.Length; i++) {
			toActivate[i].SetActive(true);
		}
		audioSrc.audio.Play();
	}
}
