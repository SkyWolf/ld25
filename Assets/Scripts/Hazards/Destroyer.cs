using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour {
	private GameObject wall;
	private GameObject wallCounter;
	
	void Start() {
		wall = GameObject.Find("Wall");	
		wallCounter = GameObject.Find("WallCounter");
	}
	
	void Update() {
		if(wall != default(GameObject) && transform.position.x < wall.transform.position.x)
			Destroy(gameObject, 1);
		else if(wallCounter != default(GameObject) && transform.position.x > wallCounter.transform.position.x)
			Destroy(gameObject, 1);
	}
	
}
