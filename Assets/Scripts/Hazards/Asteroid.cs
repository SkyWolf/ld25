using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour, IBounds, IID {
	public int spawnCost;
	
	public GameObject[] possibleShapes;
	private System.Random rand;
	
	private GameObject chosenAsteroid;
	
	void Start () {
		rand = new System.Random();
		int whichToSpawn = rand.Next(3);
		chosenAsteroid = (GameObject)Instantiate(possibleShapes[whichToSpawn], transform.position, possibleShapes[whichToSpawn].transform.rotation);
		chosenAsteroid.transform.parent = transform;
		
		Vector3 newTorque = new Vector3(rand.Next(-1,2), rand.Next(-1,2), rand.Next(-1,2));
		chosenAsteroid.constantForce.torque = newTorque;
		
		double angularDrag = (rand.NextDouble() * 0.5) + 0.5;
		chosenAsteroid.rigidbody.angularDrag = (float)angularDrag;
	}
		
	public int GetID() {
		return 0;
	}
	
	public Bounds GetBounds() {
		if(chosenAsteroid != null && chosenAsteroid.transform.FindChild("Collider") != null)
			return chosenAsteroid.transform.FindChild("Collider").gameObject.collider.bounds;
		return default(Bounds);
	}
}