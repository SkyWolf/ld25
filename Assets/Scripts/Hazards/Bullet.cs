using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour, IBounds {
	public Bounds GetBounds() {
		if(transform.FindChild("Body") == null || transform.FindChild("Body").FindChild("Collider") == null)
			return default(Bounds);
		return transform.FindChild("Body").FindChild("Collider").gameObject.collider.bounds;
	}
}
