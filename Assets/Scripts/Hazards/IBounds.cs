using UnityEngine;

public interface IBounds {
	Bounds GetBounds();
}
