using UnityEngine;
using System.Collections;

public class BasicShip : MonoBehaviour, IID {
	
	private GameObject upCannon;
	private GameObject downCannon;
	private GameObject frontCannon;
	
	public int howMany;
	public float fireRate;
	public float bulletSpeed;
	public GameObject bulletPrefab;
	
	public bool sphere;
	
	private GameObject hazards;
	
	private float elapsedTime;
	private GameObject[] activeCannons;
	
	void Start () {
		hazards = GameObject.Find("Hazards");
		
		GameObject cannonContainer = transform.FindChild("Cannon Container").gameObject;
		upCannon = cannonContainer.transform.FindChild("CannonUp").gameObject;
		downCannon = cannonContainer.transform.FindChild("CannonDown").gameObject;
		frontCannon = cannonContainer.transform.FindChild("Cannon").gameObject;
		
		activeCannons = new GameObject[howMany];
		switch(howMany) {
			case 1:	
				activeCannons[0] = frontCannon;
				break;
			case 2:
				activeCannons[0] = upCannon;
				activeCannons[1] = downCannon;
				break;
			case 3:
				activeCannons[0] = frontCannon;
				activeCannons[1] = upCannon;
				activeCannons[2] = downCannon;
				break;
		}
		
		Vector3 force = constantForce.force;
		if(Random.value <= 0.5)
			force.y *= -1;
		constantForce.force = force;
	}
	
	void Update () {
		if(elapsedTime >= fireRate) {
			for(int i=0; i<activeCannons.Length; i++) {
				GameObject bullet = (GameObject)Instantiate(bulletPrefab, activeCannons[i].transform.position, activeCannons[i].transform.rotation);
				bullet.transform.parent = hazards.transform;
				bullet.transform.FindChild("Body").gameObject.rigidbody.AddRelativeForce(bullet.transform.right * -1 * bulletSpeed, ForceMode.Impulse);
				audio.Play();
			}
			elapsedTime = 0;
		}
		elapsedTime += Time.deltaTime;	
	}
	
	public int GetID() {
		if(sphere)
			return 3 + howMany;
		return howMany;	
	}
}
