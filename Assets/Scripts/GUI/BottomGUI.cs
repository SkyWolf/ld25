using UnityEngine;
using System.Collections;

public class BottomGUI : MonoBehaviour {
	
	public Texture2D bottomGUI;
	public Texture2D topGUI;
	public Texture2D selectedGUI;
	private Rect bottomGUIRect;
	private Rect[] bottomText;
	private Rect[] selectorGUI;
	private Rect topGUIRect;
	private Rect villainGoldRect;
	
	public GameObject globalVariables;
	private HazardList hl;
	public GameObject villain;
	private VillainAttributes va;
	
	public GUISkin skin;
	
	private float screenW, screenH;
	
	void Start () {
		screenW = Screen.width;
		screenH = Screen.height;
		RecalculateRectangles();
		
		hl = (HazardList)globalVariables.GetComponent("HazardList");
		va = (VillainAttributes)villain.GetComponent("VillainAttributes");
	}
	
	void OnGUI() {
		if(Screen.width != screenW)
			screenW = Screen.width;
		if(Screen.height != screenH)
			screenH = Screen.height;
		
		RecalculateRectangles();
		
		GUI.DrawTexture(bottomGUIRect, bottomGUI);
		GUI.DrawTexture(topGUIRect, topGUI);
		
		GUI.Label(villainGoldRect,"" + va.gold,skin.label);
		for(int i=0; i<bottomText.Length; i++)
			GUI.Label(bottomText[i], "" + hl.listOfHazards[i].cost, skin.customStyles[0]);
		
		GUI.DrawTexture(selectorGUI[hl.currentlySelected], selectedGUI);
	}
	
	void RecalculateRectangles() {
		#region topTexture
		topGUIRect = new Rect(	screenW - screenW * 0.25f,
								0,
								screenW * 0.25f,
								screenH * 0.055556f	);
		#endregion
		#region topText
		villainGoldRect = new Rect(	screenW - screenW * 0.166667f,
									0,
									screenW * 0.166667f,
									screenH * 0.055556f	);
		#endregion
		#region bottomTexture
		bottomGUIRect = new Rect(	0,
									screenH - screenH * 0.155556f,
									screenW,
									screenH * 0.155556f	);
		#endregion
		#region bottomText
		float btWidth = 0.0578125f * Screen.width;
		float btHeight = 0.030556f * Screen.height;
		float btY = 0.955556f * Screen.height;
		bottomText = new Rect[7];
		bottomText[0] = new Rect(	0.108854f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		bottomText[1] = new Rect(	0.1921875f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		bottomText[2] = new Rect(	0.275520f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		bottomText[3] = new Rect(	0.358854f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		bottomText[4] = new Rect(	0.4421875f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		bottomText[5] = new Rect(	0.525520f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		bottomText[6] = new Rect(	0.608854f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		#endregion
		#region selector
		btWidth = 0.025520f * Screen.width;
		btHeight = 0.045370f * Screen.height;
		btY = screenH - screenH * 0.155556f;
		selectorGUI = new Rect[7];
		selectorGUI[0] = new Rect(	0.083333f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		selectorGUI[1] = new Rect(	0.166667f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		selectorGUI[2] = new Rect(	0.25f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		selectorGUI[3] = new Rect(	0.333333f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		selectorGUI[4] = new Rect(	0.416667f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		selectorGUI[5] = new Rect(	0.5f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		selectorGUI[6] = new Rect(	0.583333f * Screen.width,
									btY,
									btWidth,
									btHeight	);
		#endregion
	}
}
