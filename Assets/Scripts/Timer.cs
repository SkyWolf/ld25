using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {
	
	public GameObject infos;
	private bool iActive;
	
	private float elapsedTime;
	
	void Start () {
		iActive = true;
		infos.SetActive(iActive);
		
		elapsedTime = 0;
		guiText.text = ToString();
	}
	
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			Application.LoadLevel("main");	
		}
		else if(Input.GetKeyDown(KeyCode.Tab)) {
			iActive = !iActive;
			infos.SetActive(iActive);
		}
		
		elapsedTime += Time.deltaTime;
		guiText.text = ToString();
	}
	
	public override string ToString() {
		string toReturn = (int)(elapsedTime / 60) + ":";
		int seconds = (int)elapsedTime % 60;
		if(seconds < 10)
			toReturn = toReturn + "0";
		toReturn = toReturn + seconds + ".";
		int milliseconds = (int)((elapsedTime - (int)elapsedTime) * 1000);
		if(milliseconds < 100) {
			toReturn = toReturn + "0";
			if(milliseconds < 10)
				toReturn = toReturn + "0";
		}
		toReturn = toReturn + milliseconds;
		
		return toReturn;
	}
}
