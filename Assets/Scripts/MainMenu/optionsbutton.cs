using UnityEngine;
using System.Collections;

public class optionsbutton : MonoBehaviour {
	
	public GameObject optionsMenu;
	private bool mOpened = false;
	
	void Start () {
		optionsMenu.SetActive(mOpened);
	}
	
	void OnMouseDown() {
		mOpened = !mOpened;
		optionsMenu.SetActive(mOpened);
	}
}
