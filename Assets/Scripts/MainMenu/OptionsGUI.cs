using UnityEngine;
using System.Collections;
public class OptionsGUI : MonoBehaviour {

	private Rect windowRect;
	private Rect musicSBRect;
	private Rect musicLRect;
	private Rect sfxSBRect;
	private Rect sfxLRect;
	
	private float oldH, oldW;
	
	void Start() {
		oldW = Screen.width;
		oldH = Screen.height;
		RecalculateRectangles();
	}
	
	void OnGUI() {
		if(oldW != Screen.width || oldH != Screen.height) {
			oldW = Screen.width;
			oldH = Screen.height;
			RecalculateRectangles();
		}
		windowRect = GUI.Window (0, windowRect, WindowFunction, "Options");
	}

	void WindowFunction(int windowID) {
		Options.musicVolume = GUI.HorizontalScrollbar (musicSBRect, Options.musicVolume, 0.1f, 0.0f, 1.0f);
		GUI.Label(musicLRect, "Music Volume");
		
		Options.sfxVolume = GUI.HorizontalScrollbar (sfxSBRect, Options.sfxVolume, 0.1f, 0.0f, 1.0f);
		GUI.Label(sfxLRect, "Sound Effects Volume");
	}
	
	private void RecalculateRectangles() {
		windowRect = new Rect (0.049455f * Screen.width, 0.325131f * Screen.height, 0.504451f * Screen.width, 0.623901f * Screen.height);
		musicSBRect = new Rect(0.1f * windowRect.width, 0.3f * windowRect.height, 0.8f * windowRect.width, 0.2f * windowRect.height);
		musicLRect = new Rect(0.1f * windowRect.width, 0.25f * windowRect.height, 0.8f * windowRect.width, 0.1f * windowRect.height);
		sfxSBRect = new Rect(0.1f * windowRect.width, 0.7f * windowRect.height, 0.8f * windowRect.width, 0.2f * windowRect.height);
		sfxLRect = new Rect(0.1f * windowRect.width, 0.65f * windowRect.height, 0.8f * windowRect.width, 0.1f * windowRect.height);
	}
}