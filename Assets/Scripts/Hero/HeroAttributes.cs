using UnityEngine;
using System.Collections;

public class HeroAttributes : MonoBehaviour {
	public int remainingLife;
	
	private Lifebar li;
	
	void Start() {
		remainingLife = 100;
		li = (Lifebar)transform.FindChild("Life Indicator").GetComponent("Lifebar");
	}
	
	void Update() {
		li.percentage = remainingLife;
	}
	
}
