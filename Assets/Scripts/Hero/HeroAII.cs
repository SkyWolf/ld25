using UnityEngine;
using System.Collections.Generic;

public class HeroAII : MonoBehaviour {
	public GameObject hazards;
	//private List<Bounds> bounds;
	private Interval interval;
		
	private float shipSize;
	private float lerpingFrom, lerpingTo, lerpingElapsed, lerpingTime;
	private bool lerping;
	
	private const float UNITS_PER_SECOND = 8;
	
	void Start () {
		shipSize = collider.bounds.extents.y * 2;
		lerpingFrom = lerpingTo = lerpingElapsed = lerpingTime = 0;
		lerping = false;
	}
	
	void Update () {
		bool safe = true;
		
		interval = new Interval(-10, 10);
		for(int i=0; i<hazards.transform.childCount; i++) {
			IBounds ib = ((IBounds)hazards.transform.GetChild(i).GetComponent("IBounds"));
			if(ib != null) {
				Bounds b = ib.GetBounds();
				
				Vector3 min = b.center;
				min.y = b.center.y - b.extents.y;
				Vector3 max = b.center;
				max.y = b.center.y + b.extents.y;
				
				if(b.center.x - b.extents.x >= transform.position.x - (shipSize / 2)) {
					interval.Subtract(new Interval.Couple(min.y, max.y));
					if(transform.position.y - (shipSize / 2) >= min.y && transform.position.y - (shipSize / 2) <= max.y)
						safe = false;
					if(transform.position.y + (shipSize / 2) >= min.y && transform.position.y + (shipSize / 2) <= max.y)
						safe = false;
				}
			}
		}
		
		if(!safe) {
			Interval.Couple safeSpotUpwards = null;
			Interval.Couple safeSpotDownwards = null;
			
			foreach(Interval.Couple c in interval.intervals) {
				if(c.end <= transform.position.y && c.GetLength() > shipSize * 2) {
					if(safeSpotDownwards == null) safeSpotDownwards = c;
					else if(c.end > safeSpotDownwards.end)
						safeSpotDownwards = c;
				}
				if(c.start >= transform.position.y && c.GetLength() > shipSize * 2){
					if(safeSpotUpwards == null) safeSpotUpwards = c;
					else if(c.start < safeSpotUpwards.start)
						safeSpotUpwards = c;
				}
			}
			
			if(safeSpotDownwards != null || safeSpotUpwards != null) {
				float chosenYPosition;
				if(safeSpotDownwards == null)
					chosenYPosition = ((safeSpotUpwards.end - safeSpotUpwards.start) / 2) + safeSpotUpwards.start;
				else if(safeSpotUpwards == null)
					chosenYPosition = ((safeSpotDownwards.end - safeSpotDownwards.start) / 2) + safeSpotDownwards.start;
				else {
					float upwardM = ((safeSpotUpwards.end - safeSpotUpwards.start) / 2) + safeSpotUpwards.start;
					float downwardM = ((safeSpotDownwards.end - safeSpotDownwards.start) / 2) + safeSpotDownwards.start;
				
					if(Mathf.Abs(upwardM - transform.position.y) < Mathf.Abs(downwardM - transform.position.y))
						chosenYPosition = upwardM;	
					else chosenYPosition = downwardM;
				}
				
				if(!lerping) {
					lerpingFrom = transform.position.y;
					lerpingTo = chosenYPosition;
					lerpingElapsed = 0;
					lerpingTime = Mathf.Abs(lerpingFrom - lerpingTo) / UNITS_PER_SECOND;
					lerping = true;
				}
			}
			//TODO: no safe spot
			/*else {
				Debug.Log ("...");
			}*/
		}
		
		
		
		if(lerping) {
			lerpingElapsed += Time.deltaTime;
			Vector3 heroPosition = transform.position;
			heroPosition.y = Mathf.Lerp(lerpingFrom, lerpingTo, lerpingElapsed / lerpingTime);
			transform.position = heroPosition;
			
			if(lerpingElapsed >= lerpingTime)
				lerping = false;
		}
	}
}
