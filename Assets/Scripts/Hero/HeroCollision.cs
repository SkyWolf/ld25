using UnityEngine;
using System.Collections;

public class HeroCollision : MonoBehaviour {
	public GameObject explosionPrefab;
	private HeroAttributes ab;
	public GameObject globalVariables;
	private EndingScript es;
	
	void Start() {
		ab = (HeroAttributes)GetComponent("HeroAttributes");
		es = (EndingScript)globalVariables.GetComponent("EndingScript");
	}
	
	void OnCollisionEnter(Collision collision) {
		audio.Play();
		GameObject explosion = (GameObject)Instantiate(explosionPrefab, collision.gameObject.transform.position, explosionPrefab.transform.rotation);
		Destroy(collision.gameObject);
		Destroy(explosion,3);
		ab.remainingLife -= 10;
		if(ab.remainingLife <= 0) {
			GameObject endingExplosion = (GameObject)Instantiate(explosionPrefab, transform.position, explosionPrefab.transform.rotation);
			Destroy(this.gameObject);
			Destroy(endingExplosion,3);
			es.enabled = true;
		}
	}
}