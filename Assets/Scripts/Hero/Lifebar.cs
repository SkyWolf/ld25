using UnityEngine;
using System.Collections;

public class Lifebar : MonoBehaviour {
	
	public int percentage;
	private Transform foreground;
	
	void Start() {
		foreground = transform.FindChild("FG");
	}
	
	void Update () {
		if(percentage >= 0) {
			Vector3 pos = foreground.localPosition;
			pos.x = -5 + (percentage * 0.05f);
			Vector3 sca = foreground.localScale;
			sca.x = percentage * 0.01f;
			
			foreground.localPosition = pos;
			foreground.localScale = sca;
		}
	}
}
