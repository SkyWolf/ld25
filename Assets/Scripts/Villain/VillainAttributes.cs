using UnityEngine;
using System.Collections;

public class VillainAttributes : MonoBehaviour {
	
	public int goldPerSecond = 50;
	public int gold { get; private set; }
	public int goldCap;
	private float goldF;
	
	void Start () {
		gold = 150;
		goldF = 150;
	}
	
	void Update () {
		if(gold >= goldCap) {
			goldF = goldCap;
			gold = goldCap;
		}
		else {
			goldF += (goldPerSecond * Time.deltaTime);
			gold = (int)goldF;
		}
	}
	
	public void AddMoney(int amount) {
		goldF += amount;
		gold += amount;
	}
	
	public bool DetractMoney(int amount) {
		if(gold >= amount) {
			goldF -= amount;
			gold -= amount;
			return true;
		}
		return false;
	}
}