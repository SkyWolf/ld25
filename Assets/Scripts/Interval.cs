using UnityEngine;
using System.Collections.Generic;

public class Interval {
	public class Couple {
		public float start {get; private set;}
		public float end {get; private set;}
		
		public Couple(float s, float e) {
			start = s;
			end = e;
		}
		
		public void SetStart(float newStart) {
			start = newStart;
		}
		
		public void SetEnd(float newEnd) {
			end = newEnd;
		}
		
		public float GetLength() {
			return end - start;
		}
		
		public bool Contains(Couple c) {
			if(c.start >= start && c.end <= end)
				return true;
			return false;
		}
		
		public override string ToString() {
			return start + ", " + end;	
		}
	}
	
	public List<Couple> intervals;
	
	public Interval(float s, float e) {
		intervals = new List<Couple>();
		intervals.Add(new Couple(s, e));
	}
	
	public void Subtract(Couple c) {
		Couple minCouple = null;
		float minCoupleLength = float.PositiveInfinity;
		foreach (Couple i in intervals) {
			if(i.Contains(c) && i.GetLength() < minCoupleLength) {
				minCoupleLength = i.GetLength();
				minCouple = i;
			}
		}
		if(minCouple != null) {
			float temp = minCouple.end;
			minCouple.SetEnd(c.start);
			c.SetStart(c.end);
			c.SetEnd(temp);
			
			intervals.Add(c);
		}
		else {	
			Couple nearestStartOwner = null;
			float nearestEnd = c.start;
			Couple nearestEndOwner = null;
			float nearestStart = c.end;
			foreach (Couple i in intervals) {
				if(i.end > nearestEnd && i.end < c.end) {
					nearestEnd = i.end;
					nearestEndOwner = i;
				}
				if(i.start < nearestStart && i.start > c.start) {
					nearestStart = i.start;
					nearestStartOwner = i;
				}
			}
			
			if(nearestStartOwner != null) {
				if(nearestStartOwner.end < c.end)
					intervals.Remove(nearestStartOwner);
				else
					nearestStartOwner.SetStart(c.end);
			}
			if(nearestEndOwner != null) {
				if(nearestEndOwner.start > c.start)
					intervals.Remove(nearestEndOwner);
				else
					nearestEndOwner.SetEnd(c.start);
			}
		}
	}
}
