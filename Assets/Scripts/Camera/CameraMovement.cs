using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	private const float VELOCITY = 6;
	private GameObject starfield;
	private GameObject starfieldBack;
	
	void Start () {
		starfield = transform.FindChild("Starfield").gameObject;
		starfieldBack = transform.FindChild("Starfield(Back)").gameObject;
	}
	
	void Update () {
		Vector3 cameraPosition = transform.position;
		cameraPosition.x += VELOCITY * Time.deltaTime;
		transform.position = cameraPosition;
		
		Vector2 mTO = starfield.renderer.material.mainTextureOffset;
		mTO.x -= Time.deltaTime / VELOCITY * 2;
		starfield.renderer.material.mainTextureOffset = mTO;
		
		mTO = starfieldBack.renderer.material.mainTextureOffset;
		mTO.x -= Time.deltaTime / VELOCITY;
		starfieldBack.renderer.material.mainTextureOffset = mTO;
	}
}
