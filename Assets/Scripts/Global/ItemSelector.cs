using UnityEngine;
using System.Collections;

public class ItemSelector : MonoBehaviour {
	
	public GameObject selected;

	public GameObject asteroid;
	public GameObject basicShip;
	public GameObject sphereShip;
	
	private HazardList hl;
	
	void Start() {
		hl = (HazardList)GetComponent("HazardList");
	}
	
	void Update () {
		if(Input.GetKeyUp(KeyCode.Q)) {
			selected = asteroid;
			hl.currentlySelected = 0;
		}
		else if(Input.GetKeyUp(KeyCode.W)) {
			selected = basicShip;
			((BasicShip)basicShip.GetComponent("BasicShip")).howMany = 1;
			hl.currentlySelected = 1;
		}
		else if(Input.GetKeyUp(KeyCode.E)) {
			selected = basicShip;
			((BasicShip)basicShip.GetComponent("BasicShip")).howMany = 2;
			hl.currentlySelected = 2;
		}
		else if(Input.GetKeyUp(KeyCode.R)) {
			selected = basicShip;
			((BasicShip)basicShip.GetComponent("BasicShip")).howMany = 3;
			hl.currentlySelected = 3;
		}
		else if(Input.GetKeyUp(KeyCode.T)) {
			selected = sphereShip;
			((BasicShip)sphereShip.GetComponent("BasicShip")).howMany = 1;
			hl.currentlySelected = 4;
		}
		else if(Input.GetKeyUp(KeyCode.Y)) {
			selected = sphereShip;
			((BasicShip)sphereShip.GetComponent("BasicShip")).howMany = 2;
			hl.currentlySelected = 5;
		}
		else if(Input.GetKeyUp(KeyCode.U)) {
			selected = sphereShip;
			((BasicShip)sphereShip.GetComponent("BasicShip")).howMany = 3;
			hl.currentlySelected = 6;
		}
	}
}
