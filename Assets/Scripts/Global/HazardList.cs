using UnityEngine;
using System.Collections;

public class HazardList : MonoBehaviour {
	[System.Serializable]
	public class Hazard {
		public GameObject actualObject;
		public string name;
		public float cost;
	}
	
	public Hazard[] listOfHazards;
	public int currentlySelected = 0;
}
