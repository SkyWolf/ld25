using UnityEngine;
using System.Collections;

public class AudioSourceInitializer : MonoBehaviour {
	
	public GameObject[] musicAudioSources;
	public GameObject[] sfxAudioSources;
	
	void Start () {
		for(int i=0; i<musicAudioSources.Length; i++) {
			musicAudioSources[i].audio.volume = Options.musicVolume;	
		}
		for(int i=0; i<sfxAudioSources.Length; i++) {
			sfxAudioSources[i].audio.volume = Options.sfxVolume;	
		}
	}
}
